﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListaStudenti
{
    public class Student
    {

        public string nume;
        public string prenume;
        public int varsta;

        public override string ToString()
        {
            return $"{nume} {prenume} ({varsta})";
        }
    }
}
