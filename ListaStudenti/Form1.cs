﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ListaStudenti
{
    public partial class Form1 : Form
    {
        int indexToUpdate = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void btnAdauga_Click(object sender, EventArgs e)
        {
            if (txtNume.Text == "")
            {
                MessageBox.Show("Va rugam specificati numele", "Eroare", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (txtPrenume.Text == "")
            {
                MessageBox.Show("Va rugam specificati prenumele", "Eroare", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (txtVarsta.Text == "")
            {
                MessageBox.Show("Va rugam specificati varsta", "Eroare", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            // cream un nou student
            Student s = new Student();
            s.nume = txtNume.Text;
            s.prenume = txtPrenume.Text;
            s.varsta = int.Parse(txtVarsta.Text);

            // il adaugam in lista
            lstStudenti.Items.Add(s);

            txtNume.Clear();
            txtPrenume.Clear();
            txtVarsta.Clear();
        }

        private void btnDetalii_Click(object sender, EventArgs e)
        {
            // elementele din lista sunt obiecte de tip Student
            // elementul selectat va fi tot o clasa Student
            // 
            // convertim SelectedItem intr-un obiect de tip Student
            Student s = (Student) lstStudenti.SelectedItem;

            MessageBox.Show(s.nume + " " + s.prenume);
        }

        public void ActualizeazaStudent(Student s)
        {
            MessageBox.Show(s.ToString());
            lstStudenti.Items[indexToUpdate] = s;
        }

        private void btnAfisareVarsta_Click(object sender, EventArgs e)
        {
            foreach(Student s in lstStudenti.Items)
            {
                if (s.varsta > 20)
                {
                    MessageBox.Show(s.ToString());
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Student s = (Student) lstStudenti.SelectedItem;
            indexToUpdate = lstStudenti.SelectedIndex;

            ModificaStudent form = new ModificaStudent(this, s);
            form.Show();
        }

    }
}
