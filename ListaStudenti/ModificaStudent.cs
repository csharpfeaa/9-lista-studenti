﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ListaStudenti
{
    public partial class ModificaStudent : Form
    {
        Form1 form1;

        public ModificaStudent(Form1 f, Student s)
        {
            InitializeComponent();

            this.form1 = f;

            txtNume.Text = s.nume;
            txtPrenume.Text = s.prenume;
            txtVarsta.Text = "" + s.varsta;
        }

        private void btnAdauga_Click(object sender, EventArgs e)
        {
            if (txtNume.Text == "")
            {
                MessageBox.Show("Va rugam specificati numele", "Eroare", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (txtPrenume.Text == "")
            {
                MessageBox.Show("Va rugam specificati prenumele", "Eroare", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (txtVarsta.Text == "")
            {
                MessageBox.Show("Va rugam specificati varsta", "Eroare", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Student s = new Student();
            s.nume = txtNume.Text;
            s.prenume = txtPrenume.Text;
            s.varsta = int.Parse(txtVarsta.Text);

            form1.ActualizeazaStudent(s);
            this.Close();
        }
    }
}
